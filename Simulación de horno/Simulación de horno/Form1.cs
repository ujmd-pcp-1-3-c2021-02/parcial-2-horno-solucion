﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simulación_de_horno
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void nudTemperatura_ValueChanged(object sender, EventArgs e)
        {
            panelMercurio.Height = (int)(nudTemperatura.Value - 26) * panelTermómetro.Height / (180 - 26);

            pictureBox1.BackColor = Color.FromArgb(64, 0, 0);
            pictureBox2.BackColor = Color.FromArgb(64, 0, 0);
            pictureBox3.BackColor = Color.FromArgb(64, 0, 0);
            pictureBox4.BackColor = Color.FromArgb(64, 0, 0);
            pictureBox5.BackColor = Color.FromArgb(64, 0, 0);
            pictureBox6.BackColor = Color.FromArgb(64, 0, 0);
            pictureBox7.BackColor = Color.FromArgb(64, 0, 0);
            pictureBox8.BackColor = Color.FromArgb(64, 0, 0);
            pictureBox9.BackColor = Color.FromArgb(64, 0, 0);

            if (nudTemperatura.Value >= 180)
            {
                pictureBox9.BackColor = Color.Red;
            }
            if (nudTemperatura.Value >= 160)
            {
                pictureBox8.BackColor = Color.Red;
            }
            if (nudTemperatura.Value >= 140)
            {
                pictureBox7.BackColor = Color.Red;
            }
            if (nudTemperatura.Value >= 120)
            {
                pictureBox6.BackColor = Color.Red;
            }
            if (nudTemperatura.Value >= 100)
            {
                pictureBox5.BackColor = Color.Red;
            }
            if (nudTemperatura.Value >= 80)
            {
                pictureBox4.BackColor = Color.Red;
            }
            if (nudTemperatura.Value >= 60)
            {
                pictureBox3.BackColor = Color.Red;
            }
            if (nudTemperatura.Value >= 40)
            {
                pictureBox2.BackColor = Color.Red;
            }
            if (nudTemperatura.Value >= 20)
            {
                pictureBox1.BackColor = Color.Red;
            }

        }

        private void btnEncender_Click(object sender, EventArgs e)
        {
            tmrON.Enabled = true;
            tmrOFF.Enabled = false;
            pbxHornoON.Visible = true;
        }

        private void btnApagar_Click(object sender, EventArgs e)
        {
            tmrON.Enabled = false;
            tmrOFF.Enabled = true;
            pbxHornoON.Visible = false;
        }

        private void tmrON_Tick(object sender, EventArgs e)
        {
            if (nudTemperatura.Value < 180)
            {
                nudTemperatura.Value++;
            }
        }

        private void tmrOFF_Tick(object sender, EventArgs e)
        {
            if (nudTemperatura.Value > 26)
            {
                nudTemperatura.Value--;
            }
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}

﻿
namespace Simulación_de_horno
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnEncender = new System.Windows.Forms.Button();
            this.btnApagar = new System.Windows.Forms.Button();
            this.pbxHornoON = new System.Windows.Forms.PictureBox();
            this.pbxHornoOFF = new System.Windows.Forms.PictureBox();
            this.tmrON = new System.Windows.Forms.Timer(this.components);
            this.tmrOFF = new System.Windows.Forms.Timer(this.components);
            this.panelTermómetro = new System.Windows.Forms.Panel();
            this.panelMercurio = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.nudTemperatura = new System.Windows.Forms.NumericUpDown();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pbxTermómetro = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHornoON)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHornoOFF)).BeginInit();
            this.panelTermómetro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTemperatura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxTermómetro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // btnEncender
            // 
            this.btnEncender.Location = new System.Drawing.Point(29, 242);
            this.btnEncender.Name = "btnEncender";
            this.btnEncender.Size = new System.Drawing.Size(75, 23);
            this.btnEncender.TabIndex = 0;
            this.btnEncender.Text = "Encender";
            this.btnEncender.UseVisualStyleBackColor = true;
            this.btnEncender.Click += new System.EventHandler(this.btnEncender_Click);
            // 
            // btnApagar
            // 
            this.btnApagar.Location = new System.Drawing.Point(127, 242);
            this.btnApagar.Name = "btnApagar";
            this.btnApagar.Size = new System.Drawing.Size(75, 23);
            this.btnApagar.TabIndex = 1;
            this.btnApagar.Text = "Apagar";
            this.btnApagar.UseVisualStyleBackColor = true;
            this.btnApagar.Click += new System.EventHandler(this.btnApagar_Click);
            // 
            // pbxHornoON
            // 
            this.pbxHornoON.Image = global::Simulación_de_horno.Properties.Resources.Ahumador_encendido_GIF;
            this.pbxHornoON.Location = new System.Drawing.Point(56, 116);
            this.pbxHornoON.Name = "pbxHornoON";
            this.pbxHornoON.Size = new System.Drawing.Size(120, 120);
            this.pbxHornoON.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxHornoON.TabIndex = 3;
            this.pbxHornoON.TabStop = false;
            this.pbxHornoON.Visible = false;
            // 
            // pbxHornoOFF
            // 
            this.pbxHornoOFF.Image = global::Simulación_de_horno.Properties.Resources.Ahumador_apagado;
            this.pbxHornoOFF.Location = new System.Drawing.Point(56, 116);
            this.pbxHornoOFF.Name = "pbxHornoOFF";
            this.pbxHornoOFF.Size = new System.Drawing.Size(120, 120);
            this.pbxHornoOFF.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxHornoOFF.TabIndex = 2;
            this.pbxHornoOFF.TabStop = false;
            // 
            // tmrON
            // 
            this.tmrON.Tick += new System.EventHandler(this.tmrON_Tick);
            // 
            // tmrOFF
            // 
            this.tmrOFF.Tick += new System.EventHandler(this.tmrOFF_Tick);
            // 
            // panelTermómetro
            // 
            this.panelTermómetro.BackColor = System.Drawing.Color.White;
            this.panelTermómetro.Controls.Add(this.panelMercurio);
            this.panelTermómetro.Location = new System.Drawing.Point(255, 58);
            this.panelTermómetro.Name = "panelTermómetro";
            this.panelTermómetro.Size = new System.Drawing.Size(11, 301);
            this.panelTermómetro.TabIndex = 5;
            // 
            // panelMercurio
            // 
            this.panelMercurio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(11)))), ((int)(((byte)(21)))));
            this.panelMercurio.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelMercurio.Location = new System.Drawing.Point(0, 301);
            this.panelMercurio.Name = "panelMercurio";
            this.panelMercurio.Size = new System.Drawing.Size(11, 0);
            this.panelMercurio.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(41, 301);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Temperatura (°C):";
            // 
            // nudTemperatura
            // 
            this.nudTemperatura.Location = new System.Drawing.Point(137, 299);
            this.nudTemperatura.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.nudTemperatura.Minimum = new decimal(new int[] {
            26,
            0,
            0,
            0});
            this.nudTemperatura.Name = "nudTemperatura";
            this.nudTemperatura.Size = new System.Drawing.Size(55, 20);
            this.nudTemperatura.TabIndex = 7;
            this.nudTemperatura.Value = new decimal(new int[] {
            26,
            0,
            0,
            0});
            this.nudTemperatura.ValueChanged += new System.EventHandler(this.nudTemperatura_ValueChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Red;
            this.pictureBox1.Location = new System.Drawing.Point(326, 364);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(22, 13);
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox2.Location = new System.Drawing.Point(326, 325);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(22, 13);
            this.pictureBox2.TabIndex = 8;
            this.pictureBox2.TabStop = false;
            // 
            // pbxTermómetro
            // 
            this.pbxTermómetro.Image = global::Simulación_de_horno.Properties.Resources.Termómetro_26_180_Celcius;
            this.pbxTermómetro.Location = new System.Drawing.Point(220, 12);
            this.pbxTermómetro.Name = "pbxTermómetro";
            this.pbxTermómetro.Size = new System.Drawing.Size(100, 426);
            this.pbxTermómetro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxTermómetro.TabIndex = 4;
            this.pbxTermómetro.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox5.Location = new System.Drawing.Point(326, 208);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(22, 13);
            this.pictureBox5.TabIndex = 8;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox4.Location = new System.Drawing.Point(326, 247);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(22, 13);
            this.pictureBox4.TabIndex = 8;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox8.Location = new System.Drawing.Point(326, 91);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(22, 13);
            this.pictureBox8.TabIndex = 8;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox6.Location = new System.Drawing.Point(326, 169);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(22, 13);
            this.pictureBox6.TabIndex = 8;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox7.Location = new System.Drawing.Point(326, 130);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(22, 13);
            this.pictureBox7.TabIndex = 8;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox9.Location = new System.Drawing.Point(326, 52);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(22, 13);
            this.pictureBox9.TabIndex = 8;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox3.Location = new System.Drawing.Point(326, 286);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(22, 13);
            this.pictureBox3.TabIndex = 8;
            this.pictureBox3.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SeaGreen;
            this.ClientSize = new System.Drawing.Size(368, 450);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.nudTemperatura);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panelTermómetro);
            this.Controls.Add(this.pbxTermómetro);
            this.Controls.Add(this.pbxHornoON);
            this.Controls.Add(this.pbxHornoOFF);
            this.Controls.Add(this.btnApagar);
            this.Controls.Add(this.btnEncender);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Simulación de temperatura de horno";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbxHornoON)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHornoOFF)).EndInit();
            this.panelTermómetro.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudTemperatura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxTermómetro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnEncender;
        private System.Windows.Forms.Button btnApagar;
        private System.Windows.Forms.PictureBox pbxHornoOFF;
        private System.Windows.Forms.PictureBox pbxHornoON;
        private System.Windows.Forms.Timer tmrON;
        private System.Windows.Forms.Timer tmrOFF;
        private System.Windows.Forms.Panel panelTermómetro;
        private System.Windows.Forms.Panel panelMercurio;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudTemperatura;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pbxTermómetro;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}

